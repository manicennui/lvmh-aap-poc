# README #

Log in to the Google Cloud console. The user you log in as will be the owner of the new project.
Create a project in Google Cloud. By virtue of doing this your user ID will become the project owner.
Click on Billing and create or link a billing account if one is not already present.
Enable APIs. The Google Cloud Dataproc API and the Google Compute Engine API must be enabled. Use the console’s “Enable API” feature on the API Manager Dashboard, search for the API, and enable it.
Open a shell on the project. From the menu bar in the console click the “Activate Google Cloud Shell” button.
At the shell prompt type 
Git clone <repository>.git
Edit variables.txt and set
Execute the script using shell environmentSetup.sh

###

The rest of this is boiler plate

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact