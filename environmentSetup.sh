########################################################################
#!/bin/bash
#This script is  for set up of Google Cloud Project Environment with dataproc cluster
#Inputs: variables.txt
#Creation date: April 2017
#########################################################################
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

#Declare variable file
. $SCRIPTPATH/variables.txt 

export INTERNAL_ZEPPELIN_PORT=8282  #The port # that api is running on. 
export INTERNAL_DATALAB_PORT=8080  #The port # that api is running on. 
export INTERNAL_RSTUDIO_PORT=8787  #The port # that api is running on. 

do_perms=0           # 1 sets project policies, 0 does not (e.g. they already exist)
do_certificate=0     # 1 creates a certificate, 0 does not (used by load balancer)
do_cluster=0         # 1 creates the dataproc cluster, 0 does not
do_cluster_grp=1     # 1 creates the cluster instance group, 0 does not (e.g. they already exist)
do_standalone=0      # 1 creates the standalone VM, 0 does not
do_standalone_grp=0  # 1 creates the standalone instance group, 0 does not (e.g. they already exist)
do_load_balancer=1   # 1 creates the loadbalancers, 0 does not; instance group must be 1
do_http_also=1       # 1 creates the port 80 front end to the load balancer, 0 does not (insecure!)
do_dns=0             # 1 creates dns records, 0 does not

##########################################=$###############################
####Set permissions for groups####
if [ $do_perms -eq 1 ]
then
  echo "Setting up policy bindings"
  gcloud projects add-iam-policy-binding $project_name \
  --member=group:lvmhanalytics@googlegroups.com  \
  --role=roles/editor

  gcloud projects add-iam-policy-binding $project_name \
  --member=group:${brand}user@googlegroups.com  \
  --role=roles/editor

  gcloud projects add-iam-policy-binding $project_name \
  --member=user:careytainan@gmail.com  \
  --role=roles/owner

#########################################################################
####Set permissions for default Compute Engine service account####
  gcloud projects add-iam-policy-binding $project_name \
  --member=serviceAccount:447583441631-compute@developer.gserviceaccount.com \
  --role=roles/editor
fi

#########################################################################
#### Create a self-signed certificate     #########
if [ $do_certificate -eq 1 ]
then
  country=US
  state=Texas
  locality=Allen
  organization=${brand}.com
  organizationalunit=Analytics
  email=administrator@${brand}.com
  domain=${brand}.com
  commonname=${brand}.com
 
  echo "Generating SSL certificate for $domain"

  #Optional
  password=dummypassword
 
  #Generate a key
  openssl genrsa -passout pass:$password -out $domain.key 2048 -noout
 
  #Remove passphrase from the key. Comment the line out to keep the passphrase
  #echo "Removing passphrase from key"
  #openssl rsa -in $domain.key -passin pass:$password -out $domain.key
 
  #Create the request
  #echo "Creating CSR"
  openssl req -new -key $domain.key -out $domain.csr -passin pass:$password \
    -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

  openssl x509 -req -days 365 -in $domain.csr -signkey $domain.key -out $domain.crt

  gcloud compute ssl-certificates create self-signed-${brand} --certificate ${brand}.com.crt --private-key ${brand}.com.key
fi
 
##########################################################################
####Create dataproc cluster####
if [ $do_cluster -eq 1 ]
then
  echo "Creating dataproc cluster with zeppelin (internal port $INTERNAL_ZEPPELIN_PORT) and datalab (internal port 8080) for brand ${brand} in zone $zone"
  echo "This can take several minutes please be patient..."
  gcloud dataproc clusters create ${brand}-cluster \
    --zone $zone \
    --master-machine-type $master_machine_type \
    --master-boot-disk-size $master_boot_disk_size \
    --num-workers $number_of_workers \
    --worker-machine-type $worker_machine_type \
    --worker-boot-disk-size $worker_boot_disk_size \
    --metadata "zeppelin-port=$INTERNAL_ZEPPELIN_PORT" \
    --initialization-actions gs://dataproc-initialization-actions/datalab/datalab.sh,gs://dataproc-initialization-actions/zeppelin/zeppelin.sh
fi

###########################################################################
####Create Load Balancer resources to enable IAP for Zeppelin
export HTTP_EXTERNAL_PORT=80    #The port on the LB clients use
export HTTPS_EXTERNAL_PORT=443   #The port on the LB clients use
export CLUSTER_INSTANCE_GRP="dataproc-group-"${brand}
export STANDALONE_INSTANCE_GRP="standalone-group-"${brand}
export HEALTH_CHECK="zeppelin-hc-"${brand}
export HEALTH_CHECK_CHECKPATH="/"
export BK_SRV_SERVICE="zeppelin"
export HTTP_PROXY_NAME="zeppelin-proxy-"${brand}
export HTTPS_PROXY_NAME="zeppelin-https-proxy-"${brand}
export URLMAP_NAME="zeppelin-lb-"${brand}
export HTTP_FW_NAME="dataproc-fw-rule-"${brand}
export ADDRESS_NAME="z-ppelin-external-ip-"${brand}
export HTTPS_FORWARD_RULE_NAME="zeppelin-https-fr-"${brand}
export HTTP_FORWARD_RULE_NAME="zeppelin-http-fr-"${brand}

export ZEPPELIN_PORT_NAME="zeppelin"
export DATALAB_PORT_NAME="datalab"
export RSTUDIO_PORT_NAME="rstudio"

if [ $do_cluster_grp -eq 1 ]
then
  echo "Creating instance group $CLUSTER_INSTANCE_GRP with instance ${brand}-cluster-m for zone $zone"
  gcloud compute instance-groups unmanaged \
    create $CLUSTER_INSTANCE_GRP --zone $zone
  gcloud compute instance-groups unmanaged add-instances \
    $CLUSTER_INSTANCE_GRP --instances ${brand}-cluster-m --zone $zone
  #The load balancing service by default looks for a service 
  # with a key of http. Build named ports for each web ui. 
  #The set-named-ports command overwrites all previous named ports 
  # so we do it once for all known apis
  echo "Creating named ports $ZEPPELIN_PORT_NAME on port $INTERNAL_ZEPPELIN_PORT"
  echo "Creating named ports $DATALAB_PORT_NAME on port $INTERNAL_DATALAB_PORT"
  echo "Creating named ports $RSTUDIO_PORT_NAME on port $INTERNAL_RSTUDIO_PORT"
  gcloud compute instance-groups unmanaged set-named-ports \
    $CLUSTER_INSTANCE_GRP --named-ports "$ZEPPELIN_PORT_NAME:$INTERNAL_ZEPPELIN_PORT,$DATALAB_PORT_NAME:$INTERNAL_DATALAB_PORT,$RSTUDIO_PORT_NAME:$INTERNAL_RSTUDIO_PORT" \
    --zone $zone
fi

if [ $do_standalone_grp -eq 1 ]
then
  echo "Creating instance group $STANDALONE_INSTANCE_GRP with instance ${brand}-cluster-m for zone $zone"
fi

if [ $do_load_balancer -eq 1 ]
then
  if [ $do_cluster_grp -eq 1 ]
  then
    gcloud compute http-health-checks create $HEALTH_CHECK \
      --check-interval 5s --healthy-threshold 2 \
      --port $INTERNAL_ZEPPELIN_PORT --timeout 5s --unhealthy-threshold 4 \
      --request-path $HEALTH_CHECK_CHECKPATH

    echo "Creating backend service $BK_SRV_SERVICE with health check $HEALTH_CHECK for instance group $CLUSTER_INSTANCE_GRP"
    gcloud compute backend-services create $BK_SRV_SERVICE \
      --http-health-checks $HEALTH_CHECK \
      --port-name $ZEPPELIN_PORT_NAME --global
    gcloud compute backend-services add-backend $BK_SRV_SERVICE \
      --instance-group $CLUSTER_INSTANCE_GRP --global --instance-group-zone $zone

    gcloud compute url-maps create $URLMAP_NAME \
      --default-service $BK_SRV_SERVICE

    if [ $do_http_also -eq 1 ]
    then
      gcloud compute target-http-proxies create \
        $HTTP_PROXY_NAME --url-map $URLMAP_NAME
    fi

    gcloud compute target-https-proxies create \
      $HTTPS_PROXY_NAME --ssl-certificate self-signed-${brand} --url-map $URLMAP_NAME
  
    #optionally create a static address to expose externally so that 
    # we can keep it if we remove the proxy.
    #to do this uncomment the 3 lines below, comment out the 'export ZCIPS' line 
    # below, and add address parameter to forwarding-rules create e.g. --address $ZIP
    #gcloud compute addresses create $ADDRESS_NAME --global
    #export ZIP=`gcloud compute addresses describe $ADDRESS_NAME \
    #  --global --format json | jq --raw-output '.address'`

    #create ephemeral IP for load balancer. to create static IP see above.
    gcloud compute forwarding-rules create $HTTPS_FORWARD_RULE_NAME \
      --global --ip-protocol "TCP" --ports $HTTPS_EXTERNAL_PORT \
      --target-https-proxy $HTTPS_PROXY_NAME 
    export ZCIPS=`gcloud compute forwarding-rules describe $HTTPS_FORWARD_RULE_NAME \
      --global --format json | jq --raw-output '.IPAddress'`

    if [ $do_http_also -eq 1 ]
    then
      gcloud compute forwarding-rules create $HTTP_FORWARD_RULE_NAME \
        --global --ip-protocol "TCP" --ports $HTTP_EXTERNAL_PORT \
        --target-http-proxy $HTTP_PROXY_NAME 
      export ZCIP=`gcloud compute forwarding-rules describe $HTTP_FORWARD_RULE_NAME \
        --global --format json | jq --raw-output '.IPAddress'`
    fi

    # this needs to be done once per project
    gcloud compute firewall-rules create $HTTP_FW_NAME --allow tcp \
      --source-ranges "130.211.0.0/22","35.191.0.0/16"
  fi
fi

###########################################################################
####Create Load Balancer resources to enable IAP for Datalab
export HEALTH_CHECK="datalab-hc-"${brand}
export HEALTH_CHECK_CHECKPATH="/tree/datalab"
export BK_SRV_SERVICE="datalab"
export HTTP_PROXY_NAME="datalab-proxy-"${brand}
export HTTPS_PROXY_NAME="datalab-https-proxy-"${brand}
export URLMAP_NAME="datalab-lb-"${brand}
export ADDRESS_NAME="datalab-external-ip-"${brand}
export HTTPS_FORWARD_RULE_NAME="datalab-https-fr-"${brand}
export HTTP_FORWARD_RULE_NAME="datalab-http-fr-"${brand}

# reuse the instance group created earlier

if [ $do_load_balancer -eq 1 ]
then
  if [ $do_cluster_grp -eq 1 ]
  then
    gcloud compute http-health-checks create $HEALTH_CHECK \
      --check-interval 5s --healthy-threshold 2 \
      --port $INTERNAL_DATALAB_PORT --timeout 5s --unhealthy-threshold 4 \
      --request-path $HEALTH_CHECK_CHECKPATH

    echo "Creating backend service $BK_SRV_SERVICE with health check $HEALTH_CHECK for instance group $CLUSTER_INSTANCE_GRP"
    gcloud compute backend-services create $BK_SRV_SERVICE \
      --http-health-checks $HEALTH_CHECK \
      --port-name $DATALAB_PORT_NAME --global
    gcloud compute backend-services add-backend $BK_SRV_SERVICE \
      --instance-group $CLUSTER_INSTANCE_GRP --global --instance-group-zone $zone

    gcloud compute url-maps create $URLMAP_NAME \
      --default-service $BK_SRV_SERVICE

    if [ $do_http_also -eq 1 ]
    then
      gcloud compute target-http-proxies create \
        $HTTP_PROXY_NAME --url-map $URLMAP_NAME
    fi

    gcloud compute target-https-proxies create \
      $HTTPS_PROXY_NAME --ssl-certificate self-signed-${brand} --url-map $URLMAP_NAME

    #optionally create a static address to expose externally so that 
    # we can keep it if we remove the proxy.
    #to do this uncomment the 3 lines below, comment out the 'export DCIPS' line 
    # below, and add address parameter to forwarding-rules create e.g. --address $DIP
    #gcloud compute addresses create $ADDRESS_NAME --global
    #export DIP=`gcloud compute addresses describe $ADDRESS_NAME \
    #  --global --format json | jq --raw-output '.address'`
    
    gcloud compute forwarding-rules create $HTTPS_FORWARD_RULE_NAME \
      --global --ip-protocol "TCP" --ports $HTTPS_EXTERNAL_PORT \
      --target-https-proxy $HTTPS_PROXY_NAME 
    export DCIPS=`gcloud compute forwarding-rules describe $HTTPS_FORWARD_RULE_NAME \
      --global --format json | jq --raw-output '.IPAddress'`

    if [ $do_http_also -eq 1 ]
    then
      gcloud compute forwarding-rules create $HTTP_FORWARD_RULE_NAME \
        --global --ip-protocol "TCP" --ports $HTTP_EXTERNAL_PORT \
        --target-http-proxy $HTTP_PROXY_NAME 
      export DCIP=`gcloud compute forwarding-rules describe $HTTP_FORWARD_RULE_NAME \
        --global --format json | jq --raw-output '.IPAddress'`
    fi
  fi
fi

if [ -n "$ZCIPS" ]
then
  echo "Zeppelin cluster is available at https://$ZCIPS but note that it may take several minutes for all connections to go live"
fi
if [ -n "$ZCIP" ]
then
  echo "Zeppelin cluster is available at http://$ZCIP but note that it may take several minutes for all connections to go live"
fi
if [ -n "$DCIPS" ]
then
  echo "Datalab cluster is available at https://$DCIPS but note that it may take several minutes for all connections to go live"
fi
if [ -n "$DCIP" ]
then
  echo "Datalab cluster is available at http://$DCIP but note that it may take several minutes for all connections to go live"
fi
if [ -n "$RCIPS" ]
then
  echo "RStudio cluster is available at https://$RCIPS but note that it may take several minutes for all connections to go live"
fi
if [ -n "$RCIP" ]
then
  echo "RStudio cluster is available at http://$RCIP but note that it may take several minutes for all connections to go live"
fi
if [ -n "$ZIPS" ]
then
  echo "Zeppelin standalone is available at https://$ZIPS but note that it may take several minutes for all connections to go live"
fi
if [ -n "$ZIP" ]
then
  echo "Zeppelin standalone is available at http://$ZIP but note that it may take several minutes for all connections to go live"
fi
if [ -n "$DIPS" ]
then
  echo "Datalab standalone is available at https://$DIPS but note that it may take several minutes for all connections to go live"
fi
if [ -n "$DIP" ]
then
  echo "Datalab standalone is available at http://$DIP but note that it may take several minutes for all connections to go live"
fi
if [ -n "$RIPS" ]
then
  echo "RStudio standalone is available at https://$RIPS but note that it may take several minutes for all connections to go live"
fi
if [ -n "$RIP" ]
then
  echo "RStudio standalone is available at http://$RIP but note that it may take several minutes for all connections to go live"
fi

if [ $do_dns -eq 1 ]
then
  echo "Creating DNS zone and a names"
  gcloud dns managed-zones create "bobbitt" --dns-name="bobbitt.info" --description="web interface zone"
  gcloud dns record-sets transaction start -z=bobbitt
  gcloud dns record-sets transaction add -z=bobbitt --name="datalab.${brand}.bobbitt.info" --type=A --ttl=300 $DIP
  gcloud dns record-sets transaction add -z=bobbitt --name="zeppelin.${brand}.bobbitt.info" --type=A --ttl=300 $IP
  gcloud dns record-sets transaction execute -z=bobbitt
fi

exit 0
